import re

# Напишите функцию для парсинга номерных знаков автомоблей Украины
# (стандарты - AА1234BB, 12 123-45AB, a12345BC) с помощью регулярных выражений.
# Функция принимает строку и возвращает None если вся строка не является номерным знаком.
# Если является номерным знаком - возвращает саму строку.


def identification_of_license_plate(string):
    if isinstance(string, str):
        template = r'^[A-Z]{2}\d{4}[A-Z]{2}$|^\d{2}\s\d{3}-\d{2}[A-Z]{2}$|^[a-z]\d{5}[A-Z]{2}$'
        definition = re.search(template, string)
        if definition is None:
            return definition
        else:
            return definition.group()
    else:
        raise TypeError('Invalid data type!')


some_string = 'AS1456BV'  # AS1456BV 45 123-89SD a12345FD
result = identification_of_license_plate(some_string)
print(result)

# Напишите класс, который выбирает из произвольного текста номерные знаки
# и возвращает их в виде пронумерованного списка с помощью регулярных выражений.


class LicensePlate:
    license_plate = None

    def __init__(self, string):
        self.license_plate = string


class LicensePlatesOfUkraine(LicensePlate):
    template = r'[A-Z]{2}\d{4}[A-Z]{2}|\d{2}\s\d{3}-\d{2}[A-Z]{2}|[a-z]\d{5}[A-Z]{2}'
    list_of_plates = {}

    def identification_of_license_plate(self):
        definition = re.findall(self.template, self.license_plate)
        for number, plates in enumerate(definition, start=1):
            self.list_of_plates[f'{number}'] = plates
        if self.list_of_plates == {}:
            return 'There is no license plate in the string!'
        else:
            return self.list_of_plates


some_string = input('Введите номерные знаки: ')  # AS1456BV 45 123-89SD a12345FD
result = LicensePlatesOfUkraine(some_string)
print(result.identification_of_license_plate())
